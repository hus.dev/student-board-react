import React, { useEffect, useState } from "react";
import Board from "./components/Board/Board";
import Login from "./components/Login/Login";
import { Route } from "react-router-dom";
import Courses from "./Pages/Courses/Courses";
import Loading from "./components/Loading/Loading";

import { BASE_URL } from "./urls";
import "./App.scss";
function App() {
  const [data, setData] = useState("");
  const [isAuth, setIsAuth] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(async () => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);

    const token = localStorage.getItem("access_token");
    const response = await fetch(`${BASE_URL}/api/score/auth/me`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });

    try {
      if (response.ok) {
        // user is Authenticated
        let result = await response.json();
        localStorage.setItem("user", JSON.stringify(result.user));
        setData(result);
        setIsAuth(true);
      } else {
        // Please login to see your dashboard
        setIsAuth(false);
      }
      // localStorage.setItem("access_token", result.token.access_token);
    } catch (error) {
      // console.log(error);
    }
  }, []);

  if (isLoading) {
    return <Loading />;
  } else if (isAuth) {
    // Authenticated
    return (
      <>
        <Route path="/" exact component={() => <Courses data={data} />} />
        <Route
          path="/courses"
          exact
          component={() => <Courses data={data} />}
        />
        <Route path="/board/:id" exact component={() => <Board />} />
      </>
    );
  } else {
    // Unauthnenticated
    return isLoading ?  <Loading /> :  <Login />;
  }
}

export default App;
