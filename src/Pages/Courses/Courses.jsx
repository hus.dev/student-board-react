import React, { useEffect, useState } from "react";
import Course from "../../components/Course/Course";
import Header from "../../components/Header/Header";
import "./Courses.scss";

function Courses({ data }) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    setCourses(data.course);
  }, []);

  return (
    <>
      <Header name={data.user.first_name} image={data.photo} />
      <div className="container">
        <div className="title">Computiq Student Board</div>
        <a className="platform" href="https://academy.computiq.tech/">
          ACADEMY
        </a>
        <hr />
        <div className="course-list">
          {courses.map((course, i) => {
            return <Course course={course} key={i} />;
          })}
        </div>
      </div>
    </>
  );
}

export default Courses;
