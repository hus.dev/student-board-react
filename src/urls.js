export let BASE_URL;
if (window.location.origin === "http://localhost:3000") {
    // local host
    BASE_URL = "http://127.0.0.1:8000";
} else {
    // on production
    BASE_URL = "https://website-backend.computiq.tech";
}