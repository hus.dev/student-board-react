import React, { useState, useEffect } from "react";
import Task from "../Task/Task";
import ScoreBoard from "../ScoreBoard/ScoreBoard";
import Header from "../Header/Header";
import "./Board.scss";
import pattrens from "./../../assets/images/pattrens.png";
import { useParams } from "react-router-dom";
import { BASE_URL } from "./../../urls";

function Board({ data }) {
  const { id } = useParams();
  const [tasks, setTasks] = useState([]);
  const [scoreList, setScoreList] = useState([]);
  const [profile, setProfile] = useState({});
  const [photo, setPhoto] = useState("");
  const [program, setProgram] = useState({});

  const getData = async () => {
    const token = localStorage.getItem("access_token");
    const response = await fetch(`${BASE_URL}/api/score/data/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    return await response.json();
  };

  const getProfile = async () => {
    const token = localStorage.getItem("access_token");
    const response = await fetch(`${BASE_URL}/api/score/auth/me`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });
    return await response.json();
  };

  useEffect(async () => {
    const result = await getData();
    const profileData = await getProfile();
    setProfile(profileData.user);
    setPhoto(profileData.photo)
    setTasks(result.program.tasks);
    setProgram(result.program);
    setScoreList(result.data)
  }, []);

  return (
    <>
      <Header
        name={profile.first_name}
        image={photo}
      />
      <div className="board container">
        <img className="pattrens" src={pattrens} alt="" />
        <span className="board-title">Computiq Score Board </span>
        <span className="course-title">{program.title}</span>
        <div className="board-grid">
          <section className="tasks-list">
            {tasks.length >=0? tasks.map((task, index) => {
              return (
                <Task
                  key={index}
                  isLast={tasks.length == index + 1 ? true : false}
                  title={task.title}
                  description={task.description}
                  subtasks={[{ title: "scratch", status: "(DONE 👏)" }]}
                />
              );
            }): ""}
          </section>
          <section className="score-board">
            <ScoreBoard scoreList={scoreList} />
          </section>
        </div>
      </div>
    </>
  );
}

export default Board;
