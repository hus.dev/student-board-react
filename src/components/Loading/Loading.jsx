import React from "react";
import Lottie from "lottie-react";
import Logo from "./../../assets/images/computiq_ae.json";
import "./style.css"

const Loading = () => 
    <div id={"loading"}>
      <Lottie animationData={Logo} loop={false} style={{width:"200px", height:"200px"}}/>
    </div>

export default Loading;
