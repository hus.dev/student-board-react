import React, { useState, useEffect } from "react";
import "./ScoreBoard.scss";
import Scores from "../Scores/Scores";
import { BASE_URL } from "../../urls";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/fontawesome-free-solid";
import { useParams } from "react-router";

function ScoreBoard() {
  const { id } = useParams();
  const [pagination, setPagination] = useState([[]]);
  const [page, setPage] = useState(0);
  const [leftActive, setLeftActive] = useState(false);
  const [rightActive, setRightActive] = useState(true);

  const handleNext = () => {
    setLeftActive(true);
    if (page >= pagination.length - 1) {
      setRightActive(false);
    } else {
      setPage(page + 1);
      setRightActive(true);
    }
    if (page === pagination.length - 2) {
      setRightActive(false);
    }
  };

  const handlePrevious = () => {
    setRightActive(true);
    if (page === 0) {
      setLeftActive(false);
    } else {
      setPage(page - 1);
      setLeftActive(true);
    }
    if (page === 1) {
      setLeftActive(false);
    }
  };

  const handlePage = (index) => {
    setPage(index);
    if (page === 1) {
      setLeftActive(false);
    } else {
      setLeftActive(true);
    }

    if (page === pagination.length) {
      setRightActive(false);
    } else {
      setRightActive(true);
    }
  };

  // convert a long 1-D array to 2-D array
  const listToMatrix = (list, elementsPerSubArray) => {
    let matrix = [],
      i,
      k;
    list = list.sort((a, b) => b["total_score"] - a["total_score"]);
    for (i = 0, k = -1; i < list.length; i++) {
      if (i % elementsPerSubArray === 0) {
        k++;
        matrix[k] = [];
      }

      matrix[k].push(list[i]);
    }

    return matrix;
  };

  useEffect(async () => {
    const token = localStorage.getItem("access_token");
    const response = await fetch(`${BASE_URL}/api/score/data/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    });

    try {
      if (response.ok) {
        // user is Authenticated
        let result = await response.json();
        setPagination(listToMatrix(result.data, 10));
      }
      // localStorage.setItem("access_token", result.token.access_token);
    } catch (error) {
      // console.log(error);
    }
  }, []);

  return (
    <>
      <h3>SCORE BOARD</h3>
      {console.log(pagination.length)}
      {pagination.length > 0
        ? pagination[page].map((score, index) => {
            return (
              <Scores
                key={index}
                image={score.user__score_profile__photo}
                order={`#${index + page * 10}`}
                name={`${score.user__first_name}.${score.user__last_name}`}
                points={score.total_score}
              />
            );
          })
        : ""}
      <div className="pagination">
        <div>
          {pagination.map((v, index) => {
            return (
              <span
                key={index}
                onClick={() => handlePage(index)}
                className={index === page ? "activate" : ""}
              >
                {index + 1}
              </span>
            );
          })}
        </div>

        <div className="controll">
          <div
            className={leftActive ? "arrow-left activate" : "arrow-left "}
            onClick={handlePrevious}
          >
            <FontAwesomeIcon icon={faChevronLeft} />
          </div>
          <div
            className={rightActive ? "arrow-right activate" : "arrow-right "}
            onClick={handleNext}
          >
            <FontAwesomeIcon icon={faChevronRight} />
          </div>
        </div>
      </div>
    </>
  );
}

export default ScoreBoard;
