import React, { useContext, useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import { BASE_URL } from "../../urls";
import pattrens from "./../../assets/images/pattrens.png"
import "./Login.scss";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const submit = async (e) => {
    e.preventDefault();

    const response = await fetch(`${BASE_URL}/api/score/auth/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      //   credentials: 'include',
      body: JSON.stringify({
        email,
        password,
      }),
    });

    try {
      let result = await response.json();
      localStorage.setItem("access_token", result.token.access_token);
      window.location.reload();
    } catch (error) {
      setMessage("The email address or password is incorrect.");
      console.log(error);
    }
  };

  return (
    <div className="login">
      <div className="control">
        <div className="welcome">
          <span>Welcome</span>
          <span className="title">To Computiq Student Board</span>
        </div>
        <form className="login-card" onSubmit={submit}>
          <h3>Sign In</h3>
          <span
            style={{
              color: "var(--s-red)",
              marginBottom: "5px",
              display: "inline-block",
            }}
          >
            {message}
          </span>
          <FloatingLabel
            controlId="floatingInput"
            label="Email address"
            className="mb-3"
          >
            <Form.Control
              type="email"
              placeholder="name@example.com"
              onChange={(e) => setEmail(e.target.value)}
            />
          </FloatingLabel>
          <FloatingLabel controlId="floatingPassword" label="Password">
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </FloatingLabel>
          <input type="submit" value="LOGIN" className="login-btn" />
        </form>
      </div>
      <div></div>
      <img className="pattrens" src={pattrens} alt="" />
    </div>
  );
}

export default Login;
