import React from "react";
import "./Scores.scss";
function Scores(props) {
  const isCurrent = () => {
    const current = JSON.parse(localStorage.getItem("user"));

    if (current.first_name + "." + current.last_name === props.name) {
      return "current";
    }
  };

  return (
    <div className={`score ${isCurrent()}`}>
      <div>
        <span className="order">{props.order}</span>
        <span className="img">{props.image}</span>
      </div>
      <span className="me">{props.name}</span>
      <span className="points">
        <b>{props.points}</b> points
      </span>
    </div>
  );
}

export default Scores;
