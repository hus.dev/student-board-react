import React, { useState } from "react";
import "./Header.scss";
import { Link } from "react-router-dom";
import logo from "./../../assets/images/logo.png";

function Header({ name, image }) {
  const [open, setOpen] = useState(false);
  const handleLogout = () => {
    localStorage.removeItem("access_token");
    window.location.reload();
  };

  return (
    <header>
      <div
        className="control"
        onClick={() => {
          setOpen(!open);
        }}
      >
        <span className="image">{image}</span>
        {name}
        {/* <span className="profile">
          <FontAwesomeIcon icon={faUserCircle} /> Profile
        </span> */}
      </div>
      <div className="middle">
        <span>
          <Link to="/">Home</Link>
        </span>
        <span onClick={handleLogout}>Logout</span>
      </div>
      <div className="logo">
        <img width="100px" src={logo} alt="" />
      </div>
    </header>
  );
}

export default Header;
