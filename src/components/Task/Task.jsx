import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp } from "@fortawesome/fontawesome-free-solid";
import "./Task.scss";

function Task(props) {
  const [open, setOpen] = useState(false);
  const [isLast, setIsLast] = useState(props.isLast);
  return (
    <div
      onClick={() => setOpen(!open)}
      className={`task-card ${open ? "" : "hide"}`}
    >
      <div className="flag"></div>
      <div className="task-head">
        <span className="task-title">{props.title}</span>
        <span className={`arrow ${open ? "" : "rotate"}`}>
          <FontAwesomeIcon icon={faChevronUp} />
        </span>
      </div>
      <p
        className="task-description"
        dangerouslySetInnerHTML={{ __html: props.description }}
      ></p>
      <div className={`line ${isLast ? "last" : ""}`}></div>
    </div>
  );
}

export default Task;
