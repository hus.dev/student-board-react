## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Student board

	
## Technologies
Project is created with:
* [React](https://reactjs.org/)
* [Vite](https://vitejs.dev/)


	
## Setup
To run this project, install it locally using npm:

1. Exute these commands
```
$ npm install
$ npm run dev
```